Welcome to the Test-Driven Development in Scala upstream repository.
This repository is maintained by George K. Thiruvathukal.

With this project, we're going to make use of a distributed workflow from the beginning. What this
means is that each of us will do our work (including George, the maintainer) in a forked repository.

We're going to use the paradigm outlined in
http://git-scm.com/book/en/Distributed-Git-Distributed-Workflows, the only difference being that 
each of us is maintaining a private fork (repository) as opposed to a public one.

In the figure, the *blessed repository* is at https://bitbucket.org/loyolachicagocs_books/scala-tdd-packt.

Once each of us has done a fork, we will create a git remote to the blessed repository (a.k.a. an
upstream repository).

Details will follow, but as soon as you get your private fork set up, please let me know. This will
allow us to see what each other are working on. 

So the new model is commit early and often. Don't worry about holding tokens and all that jazz. Just
write!